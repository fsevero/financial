from django.contrib import admin
from account.models import Account, Montly

class MontlyAdmin(admin.ModelAdmin):
  list_display = ['description', 'account', 'value', 'signal']
  list_filter = ['signal', 'account']

  def get_queryset(self, request):
    qs = super(MontlyAdmin, self).get_queryset(request)
    if request.user.is_superuser:
        return qs
    return qs.filter(account__user=request.user)

class AccountAdmin(admin.ModelAdmin):
  list_display = ['description', 'actual_balance', 'income', 'expenses', 'disponible', 'pending_cpa', 'pending_cre']

  def get_queryset(self, request):
    qs = super(AccountAdmin, self).get_queryset(request)
    if request.user.is_superuser:
        return qs
    return qs.filter(user=request.user)

admin.site.register(Account, AccountAdmin)
admin.site.register(Montly, MontlyAdmin)
