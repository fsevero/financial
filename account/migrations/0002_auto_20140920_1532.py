# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='balance_date',
            field=models.DateField(default=datetime.date(2014, 9, 20), verbose_name='Data do saldo inicial'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='account',
            name='balance',
            field=models.DecimalField(default=0, verbose_name='Saldo Inicial', max_digits=10, decimal_places=2),
        ),
    ]
