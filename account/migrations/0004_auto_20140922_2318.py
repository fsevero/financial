# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20140920_1535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='balance_date',
            field=models.DateField(default=datetime.date(2014, 9, 22), verbose_name='Data do saldo inicial'),
        ),
    ]
