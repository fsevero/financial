# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20140920_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='balance_date',
            field=models.DateField(default=datetime.date(2014, 9, 20), verbose_name='Data do saldo inicial'),
        ),
    ]
