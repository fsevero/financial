# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=250)),
                ('balance', models.DecimalField(default=0, verbose_name='Saldo', max_digits=10, decimal_places=2)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'conta',
                'verbose_name_plural': 'contas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Montly',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=250)),
                ('value', models.DecimalField(verbose_name='Valor', max_digits=10, decimal_places=2)),
                ('signal', models.CharField(max_length=1, verbose_name='Sinal', choices=[(b'R', b'Receita'), (b'D', b'Despesa')])),
                ('account', models.ForeignKey(verbose_name='Conta', to='account.Account')),
            ],
            options={
                'verbose_name': 'mensal',
                'verbose_name_plural': 'mensais',
            },
            bases=(models.Model,),
        ),
    ]
