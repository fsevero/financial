# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=250)),
            ],
            options={
                'verbose_name': 'item',
                'verbose_name_plural': 'itens',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Receivable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parcel', models.IntegerField(default=1, verbose_name='Parcela')),
                ('value', models.DecimalField(verbose_name='Valor', max_digits=10, decimal_places=2, validators=[django.core.validators.MinValueValidator(0.01)])),
                ('date', models.DateField(verbose_name='Data')),
                ('description', models.TextField(verbose_name='Descri\xe7\xe3o')),
                ('tags', models.CharField(max_length=250, verbose_name='Tags', blank=True)),
                ('received', models.BooleanField(default=False, verbose_name='Recebido?')),
                ('account', models.ForeignKey(verbose_name='Conta', to='account.Account')),
                ('item', models.ForeignKey(verbose_name='Item', to='cre.Item')),
            ],
            options={
                'verbose_name': 'receita',
                'verbose_name_plural': 'receitas',
            },
            bases=(models.Model,),
        ),
    ]
