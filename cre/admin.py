from django.contrib import admin
from cre.models import Item, Receivable

class ReceivableAdmin(admin.ModelAdmin):
  list_display = ['account', 'item', 'value', 'date', 'received']
  list_filter = ['account', 'received', 'date']

  def get_queryset(self, request):
    qs = super(ReceivableAdmin, self).get_queryset(request)
    if request.user.is_superuser:
        return qs
    return qs.filter(account__user=request.user)

admin.site.register(Item)
admin.site.register(Receivable, ReceivableAdmin)
