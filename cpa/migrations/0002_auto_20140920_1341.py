# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cpa', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='item',
            options={'verbose_name': 'item', 'verbose_name_plural': 'itens'},
        ),
        migrations.AlterModelOptions(
            name='payable',
            options={'verbose_name': 'despesa', 'verbose_name_plural': 'despesas'},
        ),
    ]
