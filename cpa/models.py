# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MinValueValidator
from account.models import Account

class Item(models.Model):
  description = models.CharField(max_length=250)

  def __unicode__(self):
    return self.description

  class Meta:
    verbose_name = "item"
    verbose_name_plural = "itens"

class Payable(models.Model):
  parcel = models.IntegerField(default=1, verbose_name=u"Parcela")
  value = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0.01)], verbose_name=u"Valor")
  date = models.DateField(verbose_name=u"Data")
  description = models.TextField(verbose_name=u"Descrição")
  tags = models.CharField(max_length=250, verbose_name=u"Tags", blank=True)
  item = models.ForeignKey(Item, verbose_name=u"Item")
  account = models.ForeignKey(Account, verbose_name=u"Conta")
  paid = models.BooleanField(default=False, verbose_name=u"Pago?")

  def __unicode__(self):
    return self.description

  class Meta:
    verbose_name = "despesa"
    verbose_name_plural = "despesas"
