from django.contrib import admin
from cpa.models import Item, Payable

class PayableAdmin(admin.ModelAdmin):
  list_display = ['account', 'item', 'value', 'date', 'paid']
  list_filter = ['account', 'paid', 'date']

  def get_queryset(self, request):
    qs = super(PayableAdmin, self).get_queryset(request)
    if request.user.is_superuser:
        return qs
    return qs.filter(account__user=request.user)

admin.site.register(Item)
admin.site.register(Payable, PayableAdmin)
